const buildTileUrl = (providerPath, z, x, y) => {
  return `${providerPath}${z}/${x}/${y}.png`;
};

const calculateRegionMapGrid = (range, initialCoords) => {
  let collectionY = [initialCoords];
  let collectionX = [];

  for (var i = 0; i < range; i++) {
    const actualI = i + 1;
    collectionY.push({
      x: initialCoords.x,
      z: initialCoords.z,
      y: initialCoords.y + actualI
    });
    collectionY.push({
      x: initialCoords.x,
      z: initialCoords.z,
      y: initialCoords.y - actualI
    });
  }
  for (var n = 0; n < collectionY.length; n++) {
    const current = collectionY[n];
    for (x = 0; x < range; x++) {
      const actualX = x + 1;
      collectionX.push({ x: current.x + actualX, y: current.y, z: current.z });
      collectionX.push({ x: current.x - actualX, y: current.y, z: current.z });
    }
  }
  return collectionY.concat(collectionX);
};

const calculateOutermostMapGrid = (nwCoords, seCoords, zoomLevel) => {
  let wholeGrid = [];

  const x1 = nwCoords.x;
  const y1 = nwCoords.y;
  const x2 = seCoords.x;
  const y2 = seCoords.y;

  for (var x = x1; x <= x2; x++) {
    for (var y = y1; y <= y2; y++) {
      wholeGrid.push({ z: zoomLevel, x, y });
    }
  }

  return wholeGrid;
};

/**
 * @param {number} lat - Latitude value (decimal)
 * @param {number} lon - Longitude valie (decimal)
 * @param {number} zoom - Zoom Value (integer)
 */
class TilesSquareCalc {
  constructor(lat, lon, zoom) {
    this.latitude = lat;
    this.longitude = lon;
    this.zoom = zoom;
  }

  _longitudeToTile() {
    return Math.floor(((this.longitude + 180) / 360) * Math.pow(2, this.zoom));
  }

  _latitudeToTile() {
    return Math.floor(
      ((1 -
        Math.log(
          Math.tan((this.latitude * Math.PI) / 180) +
            1 / Math.cos((this.latitude * Math.PI) / 180)
        ) /
          Math.PI) /
        2) *
        Math.pow(2, this.zoom)
    );
  }

  getConvertedCoordinates() {
    return {
      z: this.zoom,
      x: this._longitudeToTile(),
      y: this._latitudeToTile()
    };
  }
}

class MetaBuilder {
  constructor(tileProviderPath = "") {
    this.providerPath = tileProviderPath;
  }
}

/**
 * @param {string} tileProviderPath - String to the Tile Server Provider
 * @param {number} nwCoordsInstance - North-west pair of coordinates (TilesSquareCalc instance)
 * @param {number} seCoordsInstance - South-west pair of coordinates (TilesSquareCalc instance)
 */
class OutermostGrid extends MetaBuilder {
  constructor(tileProviderPath, nwCoordsInstance, seCoordsInstance) {
    super(tileProviderPath);

    this.nwCoordsInstance = nwCoordsInstance;
    this.seCoordsInstance = seCoordsInstance;
  }

  _buildTilesQueue() {
    const nwZoom = this.nwCoordsInstance.zoom;
    const seZoom = this.seCoordsInstance.zoom;
    if (nwZoom !== seZoom) {
      throw new ArgumentError("Expected 'nwZoom' and 'seZoom' to be equal.");
    }
    const nwTile = this.nwCoordsInstance.getConvertedCoordinates();
    const seTile = this.seCoordsInstance.getConvertedCoordinates();
    this.tilesQueue = calculateOutermostMapGrid(nwTile, seTile, nwZoom);
  }

  buildProviderStrings() {
    this._buildTilesQueue();
    let stringsArray = [];
    for (var i = 0; i < this.tilesQueue.length; i++) {
      const { z, x, y } = this.tilesQueue[i];
      stringsArray.push(buildTileUrl(this.providerPath, z, x, y));
    }
    return stringsArray;
  }
}

/* In order to build grid for map, we need to supply two pair of coordinates:
NW lat and NW lon & SE lat and SE lon. Thanks to this we can convert those using
"TilesSquareCalc" class into Cartesian coordinates, and obtain all tiles required
to build a grid.

    const nwCoords = new TilesSquareCalc(47.606973,6.016696,15);
    const seCoords = new TilesSquareCalc(45.776024,10.848567, 15);

    Code above gives us two pair of coordinates needed in order to build whole grid with tiles.

    In order to build grid, we need to create OutermostGrid instance,
    using two previously defined TilesSquareCalc instances, like so:

    const grid = new OutermostGrid("https://a.tile.openstreetmap.org/", nwCoords, seCoords);

    and then invoke "buildProviderStrings" public method on the instance to get results.
*/

// Example run:
const nwCoords = new TilesSquareCalc(47.606973,6.016696,15);
const seCoords = new TilesSquareCalc(45.776024,10.848567, 15);
const grid = new OutermostGrid("https://a.tile.openstreetmap.org/", nwCoords, seCoords);

console.log(grid.buildProviderStrings());